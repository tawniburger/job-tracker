﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JobTracker.Models;
using JobTracker.DAL;
using JobTracker.ViewModels;

namespace JobTracker.Controllers
{
    public class OpeningController : Controller
    {
        private JobTrackerContext db = new JobTrackerContext();

        // GET: /Opening/
        public ActionResult Index()
        {
            return View(db.Openings.ToList());
        }

        // GET: /Opening/Details/5
        public ActionResult Details(int? id)
        {
            var viewModel = new OpeningDetailsData();
            var openings = db.Openings
                .Include(x => x.Contacts)
                .Include(x => x.Notes)
                .Include(x => x.Todos);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Opening opening = db.Openings.Find(id);
            if (opening == null)
            {
                return HttpNotFound();
            }

            if (id != null)
            {
                ViewBag.OpeningID = id.Value;
                viewModel.Opening = opening;
                viewModel.Contacts = openings.Where(x => x.ID == id.Value).Single().Contacts;
                viewModel.Notes = openings.Where(x => x.ID == id.Value).Single().Notes;
                viewModel.Todos = openings.Where(x => x.ID == id.Value).Single().Todos; 
            }

            return View(viewModel);
        }

        // GET: /Opening/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Opening/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Title,Company,Url,Description,DateApplied,InterestLevel")] Opening opening)
        {
            if (ModelState.IsValid)
            {
                db.Openings.Add(opening);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(opening);
        }

        // GET: /Opening/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Opening opening = db.Openings.Find(id);
            if (opening == null)
            {
                return HttpNotFound();
            }
            return View(opening);
        }

        // POST: /Opening/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Title,Company,Url,Description,DateApplied,InterestLevel")] Opening opening)
        {
            if (ModelState.IsValid)
            {
                db.Entry(opening).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(opening);
        }

        // GET: /Opening/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Opening opening = db.Openings.Find(id);
            if (opening == null)
            {
                return HttpNotFound();
            }
            return View(opening);
        }

        // POST: /Opening/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Opening opening = db.Openings.Find(id);
            db.Openings.Remove(opening);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
