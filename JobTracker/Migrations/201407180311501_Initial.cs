namespace JobTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contact",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        JobTitle = c.String(),
                        Company = c.String(),
                        Phone = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Note",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        ContactMethod = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Opening",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Company = c.String(),
                        Url = c.String(),
                        Description = c.String(),
                        DateApplied = c.DateTime(nullable: false),
                        InterestLevel = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Todo",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Action = c.String(),
                        DueDate = c.DateTime(nullable: false),
                        DateCompleted = c.DateTime(nullable: false),
                        ReminderDuration = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.NoteContact",
                c => new
                    {
                        Note_ID = c.Int(nullable: false),
                        Contact_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Note_ID, t.Contact_ID })
                .ForeignKey("dbo.Note", t => t.Note_ID, cascadeDelete: true)
                .ForeignKey("dbo.Contact", t => t.Contact_ID, cascadeDelete: true)
                .Index(t => t.Note_ID)
                .Index(t => t.Contact_ID);
            
            CreateTable(
                "dbo.OpeningContact",
                c => new
                    {
                        Opening_ID = c.Int(nullable: false),
                        Contact_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Opening_ID, t.Contact_ID })
                .ForeignKey("dbo.Opening", t => t.Opening_ID, cascadeDelete: true)
                .ForeignKey("dbo.Contact", t => t.Contact_ID, cascadeDelete: true)
                .Index(t => t.Opening_ID)
                .Index(t => t.Contact_ID);
            
            CreateTable(
                "dbo.OpeningNote",
                c => new
                    {
                        Opening_ID = c.Int(nullable: false),
                        Note_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Opening_ID, t.Note_ID })
                .ForeignKey("dbo.Opening", t => t.Opening_ID, cascadeDelete: true)
                .ForeignKey("dbo.Note", t => t.Note_ID, cascadeDelete: true)
                .Index(t => t.Opening_ID)
                .Index(t => t.Note_ID);
            
            CreateTable(
                "dbo.TodoOpening",
                c => new
                    {
                        Todo_ID = c.Int(nullable: false),
                        Opening_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Todo_ID, t.Opening_ID })
                .ForeignKey("dbo.Todo", t => t.Todo_ID, cascadeDelete: true)
                .ForeignKey("dbo.Opening", t => t.Opening_ID, cascadeDelete: true)
                .Index(t => t.Todo_ID)
                .Index(t => t.Opening_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TodoOpening", "Opening_ID", "dbo.Opening");
            DropForeignKey("dbo.TodoOpening", "Todo_ID", "dbo.Todo");
            DropForeignKey("dbo.OpeningNote", "Note_ID", "dbo.Note");
            DropForeignKey("dbo.OpeningNote", "Opening_ID", "dbo.Opening");
            DropForeignKey("dbo.OpeningContact", "Contact_ID", "dbo.Contact");
            DropForeignKey("dbo.OpeningContact", "Opening_ID", "dbo.Opening");
            DropForeignKey("dbo.NoteContact", "Contact_ID", "dbo.Contact");
            DropForeignKey("dbo.NoteContact", "Note_ID", "dbo.Note");
            DropIndex("dbo.TodoOpening", new[] { "Opening_ID" });
            DropIndex("dbo.TodoOpening", new[] { "Todo_ID" });
            DropIndex("dbo.OpeningNote", new[] { "Note_ID" });
            DropIndex("dbo.OpeningNote", new[] { "Opening_ID" });
            DropIndex("dbo.OpeningContact", new[] { "Contact_ID" });
            DropIndex("dbo.OpeningContact", new[] { "Opening_ID" });
            DropIndex("dbo.NoteContact", new[] { "Contact_ID" });
            DropIndex("dbo.NoteContact", new[] { "Note_ID" });
            DropTable("dbo.TodoOpening");
            DropTable("dbo.OpeningNote");
            DropTable("dbo.OpeningContact");
            DropTable("dbo.NoteContact");
            DropTable("dbo.Todo");
            DropTable("dbo.Opening");
            DropTable("dbo.Note");
            DropTable("dbo.Contact");
        }
    }
}
