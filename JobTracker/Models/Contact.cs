﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace JobTracker.Models
{
    public class Contact
    {
        public int ID { get; set; }
        public string Name { get; set; }
        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }
        public string Company { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public virtual ICollection<Opening> Openings { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
    }
}