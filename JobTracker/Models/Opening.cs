﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace JobTracker.Models
{
    public class Opening
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Company { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        [Display(Name = "Date Applied")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateApplied { get; set; }
        [Display(Name = "Interest Level")]
        public int InterestLevel { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
        public virtual ICollection<Todo> Todos { get; set; }
    }
}