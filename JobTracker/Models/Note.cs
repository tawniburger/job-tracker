﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JobTracker.Models
{
    public class Note
    {
        public int ID { get; set; }
        public string Description { get; set; }
        [Display(Name = "Contact Method")]
        public string ContactMethod { get; set; }
        public ICollection<Opening> Openings { get; set; }
        public ICollection<Contact> Contacts { get; set; }
    }
}