﻿using JobTracker.Models;
using System.Collections.Generic;

namespace JobTracker.ViewModels
{
    public class OpeningDetailsData
    {
        public Opening Opening { get; set; }
        public IEnumerable<Contact> Contacts { get; set; }
        public IEnumerable<Note> Notes { get; set; }
        public IEnumerable<Todo> Todos { get; set; }
    }
}